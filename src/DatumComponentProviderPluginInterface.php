<?php

namespace Drupal\datum_connector;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Datum Component Discovery plugins.
 */
interface DatumComponentProviderPluginInterface extends PluginInspectionInterface {

  /**
   * Get the weight of the plugin.
   *
   * @return int
   *   The plugin's weight.
   */
  public function getWeight(): int;

  /**
   * Get the available components.
   *
   * @return array
   *   An array of all the available Datum components.
   */
  public function getComponents(): array;

  /**
   * Get the available sections.
   *
   * @return array
   *   An array of all the available Datum sections.
   */
  public function getSections(): array;

}
