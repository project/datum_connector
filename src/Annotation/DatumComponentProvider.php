<?php

namespace Drupal\datum_connector\Annotation;

use Drupal\Component\Annotation\Plugin;
use Drupal\Core\Annotation\Translation;

/**
 * Defines a DatumComponentProvider item annotation object.
 *
 * @see \Drupal\datum_connector\DatumComponentProviderManager
 * @see plugin_api
 *
 * @Annotation
 */
class DatumComponentProvider extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public string $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public Translation $label;

  /**
   * The weight of the plugin.
   *
   * @var int
   */
  public int $weight;

}
