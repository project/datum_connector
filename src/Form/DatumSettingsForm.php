<?php

namespace Drupal\datum_connector\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Define the Datum settings form.
 */
class DatumSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'datum_connector.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'datum_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('datum_connector.settings');

    $form['components_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Components Path'),
      '#description' => $this->t('Path without a leading slash to components.json relative to the Drupal web root i.e. <pre>themes/custom/mytheme/dist/components.json</pre>'),
      '#size' => 64,
      '#default_value' => $config->get('components_path'),
    ];

    $form['components_directory'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Components Directory'),
      '#description' => $this->t('Path without a leading slash to a directory to look for *.component.json files relative to the Drupal web root. The directory will be searched recursively. i.e. <pre>themes/custom/mytheme/dist/</pre>'),
      '#size' => 64,
      '#default_value' => $config->get('components_directory'),
    ];

    $form['sections_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sections Path'),
      '#description' => $this->t('Path without a leading slash to sections.json relative to the Drupal web root i.e. <pre>themes/custom/mytheme/dist/sections.json</pre>'),
      '#size' => 64,
      '#default_value' => $config->get('sections_path'),
    ];

    $form['sections_directory'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Sections Directory'),
      '#description' => $this->t('Path without a leading slash to a directory to look for *.section.json files relative to the Drupal web root. The directory will be searched recursively. i.e. <pre>themes/custom/mytheme/dist/</pre>'),
      '#size' => 64,
      '#default_value' => $config->get('sections_directory'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('datum_connector.settings')
      ->set('components_path', $form_state->getValue('components_path'))
      ->save();

    $this->config('datum_connector.settings')
      ->set('components_directory', $form_state->getValue('components_directory'))
      ->save();

    $this->config('datum_connector.settings')
      ->set('sections_path', $form_state->getValue('sections_path'))
      ->save();

    $this->config('datum_connector.settings')
      ->set('sections_directory', $form_state->getValue('sections_directory'))
      ->save();
  }

}
