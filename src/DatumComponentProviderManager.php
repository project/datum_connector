<?php

namespace Drupal\datum_connector;

use Drupal\Component\Utility\SortArray;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\datum_connector\Annotation\DatumComponentProvider;

/**
 * Manager for Datum Component Provider plugins.
 */
class DatumComponentProviderManager extends DefaultPluginManager {

  /**
   * Constructs a new DatumComponentProvider object.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct(
      'Plugin/Datum/ComponentProvider',
      $namespaces,
      $module_handler,
      DatumComponentProviderPluginInterface::class,
      DatumComponentProvider::class
    );

    $this->alterInfo('datum_component_provider_info');
    $this->setCacheBackend($cache_backend, 'datum_component_provider_plugins');
  }

  /**
   * Get an array of all components provided by plugins.
   *
   * @return array
   *   The full list of available components.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getComponents(): array {
    $providers = $this->getDefinitions();

    uasort($providers, [SortArray::class, 'sortByWeightElement']);

    $components = [];
    foreach ($providers as $provider) {
      /** @var \Drupal\datum_connector\DatumComponentProviderPluginInterface $instance */
      $instance = $this->createInstance($provider['id']);
      $components = array_merge($components, $instance->getComponents());
    }

    return $components;
  }

  /**
   * Get an array of all sections provided by plugins.
   *
   * @return array
   *   The full list of available sections.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginException
   */
  public function getSections(): array {
    $providers = $this->getDefinitions();

    uasort($providers, [SortArray::class, 'sortByWeightElement']);

    $sections = [];
    foreach ($providers as $provider) {
      /** @var \Drupal\datum_connector\DatumComponentProviderPluginInterface $instance */
      $instance = $this->createInstance($provider['id']);
      $sections = array_merge($sections, $instance->getSections());
    }

    return $sections;
  }

}
