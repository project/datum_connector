<?php

namespace Drupal\datum_connector\Plugin\Layout;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Layout\LayoutDefault;
use Drupal\Core\Plugin\PluginFormInterface;

/**
 * Provide a layout that uses an RJSF form.
 *
 * @Layout (
 *  id = "datum_section",
 *  deriver = "Drupal\datum_connector\Plugin\Derivative\DatumSectionDeriver"
 * )
 */
class DatumSection extends LayoutDefault implements PluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return parent::defaultConfiguration() + [
      'values' => new \stdClass(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form += parent::buildConfigurationForm($form, $form_state);
    $config = $this->getConfiguration();
    $plugin = $this->getPluginDefinition();

    $section = $plugin->get('section');
    $form_state->set('section', $section);

    // @TODO remove this before stable release.
    $values = $config['values'] ?? $this->defaultConfiguration()['values'];
    if (is_string($values)) {
      $values = json_decode($this->configuration['values']);
    }
    if (is_array($values) ) {
      $values = $values ? json_decode(json_encode($values)) : new \stdClass();
    }

    $form['rjsf_element'] = [
      '#type' => 'rjsf_editor',
      '#schema' => $section['schema'],
      '#uiSchema' => $section['uiSchema'] ?? [],
      '#default_value' => $values,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function build(array $regions) {
    $build = parent::build($regions);

    // @TODO remove this before stable release.
    $values = $this->configuration['values'];
    if (is_string($values)) {
      $values = json_decode($this->configuration['values']);
    }
    if (is_array($values) ) {
      $values = $values ? json_decode(json_encode($values)) : new \stdClass();
    }

    $processed = \Drupal::getContainer()->get('rjsf.render.preprocessor')->preprocess(
      $values,
      $this->pluginDefinition->get('section')['renderPreprocess'] ?? [],
      $this->pluginDefinition->get('section')['schema'],
      $this->pluginDefinition->get('section')['uiSchema'] ?? []
    );

    $build['#settings']['values'] = $processed['processed'];
    $build['#settings']['original'] = $values;

    $processed['cacheable_metadata']->applyto($build);

    return $build;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    // Decode the value while respecting the difference between objects and
    // arrays. This is particularly important when value contains an empty json
    // object {} because Json::decode or json_decode($var, TRUE) will convert
    // {} to an empty array which can cause type checking during schema
    // validation to fail.
    $this->configuration['values'] = $form_state->getValue(['rjsf_element', 'value']) ?? '{}';
  }

}
