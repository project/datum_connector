<?php

namespace Drupal\datum_connector\Plugin\Datum\ComponentProvider;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\datum_connector\Annotation\DatumComponentProvider;
use Drupal\datum_connector\DatumComponentProviderBase;
use Drupal\datum_connector\DatumComponentProviderPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provide components and sections defined at the configured paths.
 *
 * @DatumComponentProvider(
 *   id = "datum_config",
 *   label = "Datum components from path",
 *   weight = "10",
 * )
 */
class DatumConfig extends DatumComponentProviderBase implements DatumComponentProviderPluginInterface, ContainerFactoryPluginInterface {

  /**
   * The config factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $config;

  /**
   * The file system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Constructs a DatumConfig object.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config, FileSystemInterface $fileSystem) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->config = $config;
    $this->fileSystem = $fileSystem;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('file_system')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getComponents(): array {
    $filePath = $this->config->get('datum_connector.settings')->get('components_path');
    $path = DRUPAL_ROOT . '/' . $filePath;

    $components = [];

    // Load components from a specified file.
    if ($filePath && file_exists($path)) {
      $json = file_get_contents($path);
      $components = Json::decode($json);
    }


    // Load components from a specified directory.
    $directoryPath = $this->config->get('datum_connector.settings')->get('components_directory');
    $path = DRUPAL_ROOT . '/' . $directoryPath;

    if ($directoryPath && is_dir($path)) {
      $files = $this->fileSystem->scanDirectory($path, '/.*\.component\.json/', ['recurse' =>  TRUE]);

      foreach ($files as $file) {
        $json = file_get_contents($file->uri);
        $definition = Json::decode($json);
        $components[$definition['datum']['id']] = $definition;
      }
    }

    return $components;
  }

  /**
   * {@inheritdoc}
   */
  public function getSections(): array {
    $filePath = $this->config->get('datum_connector.settings')->get('sections_path');
    $path = DRUPAL_ROOT . '/' . $filePath;

    $sections = [];
    // Load layouts from a specified directory.
    if ($filePath && file_exists($path)) {
      $json = file_get_contents($path);
      $sections = Json::decode($json);
    }

    $directoryPath = $this->config->get('datum_connector.settings')->get('sections_directory');
    $path = DRUPAL_ROOT . '/' . $directoryPath;

    // Load sections from a specified directory.
    if ($directoryPath && is_dir($path)) {
      $files = $this->fileSystem->scanDirectory($path, '/.*\.section\.json/', ['recurse' =>  TRUE]);

      foreach ($files as $file) {
        $json = file_get_contents($file->uri);
        $definition = Json::decode($json);
        $sections[$definition['datum']['id']] = $definition;
      }
    }

    return $sections;
  }

}
