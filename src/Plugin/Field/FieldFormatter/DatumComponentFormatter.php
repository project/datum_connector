<?php

namespace Drupal\datum_connector\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\datum_connector\DatumComponentProviderManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Plugin implementation of the 'datum_component_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "datum_component_formatter",
 *   label = @Translation("Datum Component"),
 *   field_types = {
 *     "datum_component"
 *   }
 * )
 */
class DatumComponentFormatter extends FormatterBase {

  /**
   * The Datum provider manager.
   *
   * @var \Drupal\datum_connector\DatumComponentProviderManager
   */
  protected $datumProvider;

  /**
   * {@inheritdoc}
   */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, DatumComponentProviderManager $datumProvider) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);

    $this->datumProvider = $datumProvider;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('plugin.manager.datum.component_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $chosenComponent = $items->getFieldDefinition()->getSetting('component');

    // Match the field's component id to a full definition.
    $definition = NULL;
    foreach ($this->datumProvider->getComponents() as $component) {
      if (!$component['datum'] || !$component['datum']['id']) {
        continue;
      }

      if ($component['datum']['id'] === $chosenComponent) {
        $definition = $component;
      }
    };

    $elements = [];
    if (is_null($definition)) {
      \Drupal::logger('datum_connector')->warning('Failed to load component definition for component %id', ['%id' => $chosenComponent]);
      return $elements;
    }

    foreach ($items as $delta => $item) {
      $processed = \Drupal::getContainer()->get('rjsf.render.preprocessor')->preprocess(
        json_decode($item->value),
        $definition['renderPreprocess'] ?? [],
        $definition['schema'],
        $definition['uiSchema'] ?? []
      );

      $processed['processed']['original'] = json_decode($item->value);

      $elements[$delta] = [
        '#theme' => 'datum_component_field',
        '#values' => $processed['processed'],
        '#datum' => $definition['datum'],
      ];

      $processed['cacheable_metadata']->applyto($elements[$delta]);
    }

    return $elements;
  }

}
