<?php

namespace Drupal\datum_connector\Plugin\Field\FieldType;

use Drupal\Core\Form\FormStateInterface;
use Drupal\rjsf\Plugin\Field\FieldType\RjsfDataItem;

/**
 * Plugin implementation of the 'datum_component' field type.
 *
 * @FieldType(
 *   id = "datum_component",
 *   label = @Translation("Datum Component"),
 *   description = @Translation("Collect data for a predefined Datum component."),
 *   default_widget = "datum_component_widget",
 *   default_formatter = "datum_component_formatter",
 *   list_class = "\Drupal\datum_connector\Plugin\Field\FieldType\DatumComponentDataFieldItemList",
 * )
 */
class DatumComponentDataItem extends RjsfDataItem {

  /**
   * {@inheritdoc}
   */
  public static function defaultStorageSettings() {
    return [
      'component' => [],
    ] + parent::defaultStorageSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function storageSettingsForm(array &$form, FormStateInterface $form_state, $has_data) {
    $elements = [];

    /** @var \Drupal\datum_connector\DatumComponentProviderManager $datumProvider */
    $datumProvider = \Drupal::service('plugin.manager.datum.component_provider');

    $options = [];
    foreach ($datumProvider->getComponents() as $component) {
      if (!$component['datum'] || !$component['datum']['id']) {
        continue;
      }

      $options[$component['datum']['id']] = $component['datum']['id'];
    };

    $elements['component'] = [
      '#type' => 'select',
      '#title' => t('Component'),
      '#required' => TRUE,
      '#default_value' => $this->getSetting('component'),
      '#description' => t('Select the component to use.'),
      '#options' => $options,
    ];

    return $elements;
  }

}
