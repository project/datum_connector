<?php

namespace Drupal\datum_connector\Plugin\Field\FieldType;

use Drupal\Core\Field\MapFieldItemList;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines an item list class for datum component fields.
 */
class DatumComponentDataFieldItemList extends MapFieldItemList {

  /**
   * {@inheritdoc}
   */
  public function defaultValuesForm(array &$form, FormStateInterface $form_state) {}

}
