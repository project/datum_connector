<?php

namespace Drupal\datum_connector\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'datum_component_widget' widget.
 *
 * @FieldWidget(
 *   id = "datum_component_widget",
 *   module = "datum",
 *   label = @Translation("Datum Component Widget"),
 *   field_types = {
 *     "datum_component"
 *   }
 * )
 */
class DatumComponentWidget extends WidgetBase {

  /**
   * {@inheritdoc}
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {

    /** @var \Drupal\datum_connector\DatumComponentProviderManager $datumProvider */
    $datumProvider = \Drupal::service('plugin.manager.datum.component_provider');
    $chosenComponent = $items->getFieldDefinition()->getSetting('component');

    // Match the field's component id to a full definition.
    $definition = NULL;
    foreach ($datumProvider->getComponents() as $component) {
      if (!$component['datum'] || !$component['datum']['id']) {
        continue;
      }

      if ($component['datum']['id'] === $chosenComponent) {
        $definition = $component;
      }
    };

    if (is_null($definition)) {
      \Drupal::logger('datum_connector')->warning('Failed to load component definition for component %id', ['%id' => $chosenComponent]);
      return $element;
    }

    $element['value'] = $element + [
      '#type' => 'rjsf_editor',
      '#schema' => $definition['schema'],
      '#uiSchema' => $definition['uiSchema'],
      '#server_validation' => $items->getFieldDefinition()->getSetting('server_validation'),
      '#client_validation' => $items->getFieldDefinition()->getSetting('client_validation'),
      '#default_value' => isset($items[$delta]->value) ? json_decode($items[$delta]->value) : NULL,
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function massageFormValues(array $values, array $form, FormStateInterface $form_state) {
    foreach ($values as &$value) {
      $value = $value['value']['value'];
    }
    return $values;
  }

}
