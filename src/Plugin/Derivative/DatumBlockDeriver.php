<?php

namespace Drupal\datum_connector\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\datum_connector\DatumComponentProviderManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Derive blocks from all the provided Datum components.
 *
 * @package Drupal\datum_connector\Plugin\Derivative
 */
class DatumBlockDeriver extends DeriverBase implements ContainerDeriverInterface {

  use LoggerChannelTrait;

  /**
   * The Datum component providers.
   *
   * @var \Drupal\datum_connector\DatumComponentProviderManager
   */
  protected $componentProviders;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a new DatumBlockDeriver object.
   *
   * @param \Drupal\datum_connector\DatumComponentProviderManager $componentProviders
   *   The Datum component providers.
   */
  public function __construct(DatumComponentProviderManager $componentProviders) {
    $this->componentProviders = $componentProviders;
    $this->logger = $this->getLogger('datum_connector');
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('plugin.manager.datum.component_provider')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [];

    $components = $this->componentProviders->getComponents();
    foreach ($components as $component) {
      $schema = $component['schema'];
      $datum = $component['datum'] ?? [];

      if (!isset($datum['id']) || empty($datum['id'])) {
        $this->logger->warning('<code>datum.id</code> value missing from Datum component with the following schema: <pre><code>@schema</pre></code>', ['@schema' => json_encode($schema, JSON_PRETTY_PRINT)]);
        continue;
      }

      $this->derivatives[$datum['id']] = $base_plugin_definition;
      $this->derivatives[$datum['id']]['admin_label'] = $schema['title'];
      $this->derivatives[$datum['id']]['component'] = $component;
    }

    return $this->derivatives;
  }

}
