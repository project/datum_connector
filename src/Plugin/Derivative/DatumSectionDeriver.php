<?php

namespace Drupal\datum_connector\Plugin\Derivative;

use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Layout\LayoutDefinition;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Drupal\Core\Template\TwigEnvironment;
use Drupal\datum_connector\DatumComponentProviderManager;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Filesystem\Filesystem;
use Drupal\Core\Extension\ModuleExtensionList;

/**
 * Derive layouts from all the provided Datum sections.
 *
 * @package Drupal\datum_connector\Plugin\Derivative
 */
class DatumSectionDeriver extends DeriverBase implements ContainerDeriverInterface {

  use LoggerChannelTrait;

  /**
   * The Datum component providers.
   *
   * @var \Drupal\datum_connector\DatumComponentProviderManager
   */
  protected $componentProviders;

  /**
   * The twig environment.
   *
   * @var \Drupal\Core\Template\TwigEnvironment
   */
  protected $twig;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

   /**
  * The list of available modules.
  *
  * @var \Drupal\Core\Extension\ModuleExtensionList
  */
  protected $extensionList;

  /**
   * Constructs a new DatumSectionDeriver object.
   *
   * @param \Drupal\datum_connector\DatumComponentProviderManager $componentProviders
   *   The Datum component providers.
   * @param \Drupal\Core\Template\TwigEnvironment $twig
   *   The twig environment.
   * @param \Drupal\Core\Extension\ModuleExtensionList $extensionList
   *   The list of available modules.
   */
  public function __construct(DatumComponentProviderManager $componentProviders, TwigEnvironment $twig, ModuleExtensionList $extensionList) {
    $this->componentProviders = $componentProviders;
    $this->twig = $twig;
    $this->logger = $this->getLogger('datum_connector');
    $this->extensionList = $extensionList;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('plugin.manager.datum.component_provider'),
      $container->get('twig'),
      $container->get('extension.list.module')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {
    $this->derivatives = [];

    $sections = $this->componentProviders->getSections();
    foreach ($sections as $section) {
      $schema = $section['schema'];
      $datum = $section['datum'] ?? [];

      if (!isset($datum['id']) || empty($datum['id'])) {
        $this->logger->warning('<code>datum.id</code> value missing from Datum section with the following schema: <pre><code>@schema</pre></code>', ['@schema' => json_encode($schema, JSON_PRETTY_PRINT)]);
        continue;
      }

      if (!isset($datum['section'])) {
        $this->logger->warning('<code>datum.section</code> value missing from Datum section with the following schema: <pre><code>@schema</pre></code>', ['@schema' => json_encode($schema, JSON_PRETTY_PRINT)]);
        continue;
      }

      // The LayoutPluginManager assumes a lot about the path to a template
      // all this logic is needed to support namespaced template definitions
      // and build a path that will survive the managers' attempts at building a
      // path.
      if (substr($datum['section']['template'], 0, 1) === '@') {
        // @TODO fix this. docroot/core/lib/Drupal/Core/Template/Loader/ThemeRegistryLoader.php:50 recurses in the
        //     admin theme for some reason. Possibly related to https://www.drupal.org/project/components/issues/3107993
        $isAdminTheme = \Drupal::theme()->getActiveTheme()->getName() === \Drupal::config('system.theme')->get('admin');
        if ($this->twig->getLoader()->exists($datum['section']['template'])) {
          // Get the absolute filesystem path to the template.
          $templateContext = $this->twig->getLoader()->getSourceContext($datum['section']['template']);
          $templatePath = explode('/', $templateContext->getPath());

          // Extract the template file name.
          $template = array_pop($templatePath);

          $templatePath = DRUPAL_ROOT . '/' . implode('/', $templatePath);

          // Convert the absolute template path to one that is relative to this
          // module.
          $filesystem = new Filesystem();
          $modulePath = getcwd() . '/' . $this->extensionList->getPath('datum_connector');
          $templatePath = $filesystem->makePathRelative($templatePath, $modulePath);

          $datum['section']['path'] = $templatePath;
          // Remove the .html.twig from the template as Drupal assumes it.
          $datum['section']['template'] = str_replace('.html.twig', '', $template);
        }
      }

      $definitionArray = $datum['section'];

      // Add all the values from the base plugin to our definition.
      $definitionArray['deriver'] = $base_plugin_definition->getDeriver();
      $definitionArray['theme_hook'] = $definitionArray['theme_hook'] ?? $base_plugin_definition->getThemeHook();
      $definitionArray['id'] = $datum['id'];
      $definitionArray['class'] = $base_plugin_definition->getClass();
      $definitionArray['provider'] = $base_plugin_definition->getProvider();

      // Add the section definition to the plugin. This contains the RJSF form
      // schema definition used when creating the section in layout builder.
      $definitionArray['section'] = $section;

      $definition = new LayoutDefinition($definitionArray);
      $this->derivatives[$datum['id']] = $definition;
    }

    return $this->derivatives;
  }

}
