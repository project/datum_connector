<?php

namespace Drupal\datum_connector\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Create a Datum Component block.
 *
 * @Block(
 *  id = "datum_block",
 *  admin_label = @Translation("Datum block"),
 *  category = @Translation("Datum Components"),
 *  deriver = "Drupal\datum_connector\Plugin\Derivative\DatumBlockDeriver"
 * )
 */
class DatumBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'values' => new \stdClass(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $componentName = strtolower(str_replace(' ', '_', $this->pluginDefinition['component']['schema']['title']));

    // @TODO remove this before stable release.
    $values = $this->configuration['values'];
    if (is_array($values) ) {
      $values = $values ? json_decode(json_encode($values)) : new \stdClass();
    }
    else if (is_string($values)) {
      $values = json_decode($values);
    }

    $processed = \Drupal::getContainer()->get('rjsf.render.preprocessor')->preprocess(
      $values,
      $this->pluginDefinition['component']['renderPreprocess'] ?? [],
      $this->pluginDefinition['component']['schema'],
      $this->pluginDefinition['component']['uiSchema'] ?? []
    );

    $processed['processed']['original'] = $values;

    $component = [
      '#theme' => 'datum_component__' . $componentName,
      '#values' => $processed['processed'],
      '#datum' => $this->pluginDefinition['component']['datum'],
    ];

    $processed['cacheable_metadata']->applyto($component);

    return $component;
  }

  /**
   * {@inheritdoc}
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form += parent::blockForm($form, $form_state);
    $config = $this->getConfiguration();
    $plugin = $this->getPluginDefinition();

    $component = $plugin['component'];
    $form_state->set('component', $component);

    // @TODO remove this before stable release.
    $values = $config['values'];
    if (is_array($values) ) {
      $values = $values ? json_decode(json_encode($values)) : new \stdClass();
    }
    else if (is_string($values)) {
      $values = json_decode($values);
    }

    $form['rjsf_element'] = [
      '#type' => 'rjsf_editor',
      '#schema' => $component['schema'],
      '#uiSchema' => $component['uiSchema'] ?? [],
      '#default_value' => $values ?? $this->defaultConfiguration()['values'],
    ];

    return $form;
  }

  /**
   * Handles decoding the component data and saving it to the block.
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    // Decode the value while respecting the difference between objects and
    // arrays. This is particularly important when value contains an empty json
    // object {} because Json::decode or json_decode($var, TRUE) will convert
    // {} to an empty array which can cause type checking during schema
    // validation to fail.
    $this->configuration['values'] = $form_state->getValue(['rjsf_element', 'value']);

    parent::blockSubmit($form, $form_state);
  }

}
