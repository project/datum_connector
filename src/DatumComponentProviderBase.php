<?php

namespace Drupal\datum_connector;

use Drupal\Core\Plugin\PluginBase;

/**
 * Provides a base implementation for Datum Component Provider plugins.
 */
abstract class DatumComponentProviderBase extends PluginBase implements DatumComponentProviderPluginInterface {

  /**
   * The weight of the plugin.
   *
   * @var int
   */
  protected int $weight;

  /**
   * {@inheritdoc}
   */
  public function getWeight(): int {
    return $this->weight;
  }

  /**
   * {@inheritdoc}
   */
  public function getComponents(): array {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getSections(): array {
    return [];
  }

}
