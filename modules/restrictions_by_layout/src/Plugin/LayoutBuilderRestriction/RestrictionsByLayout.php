<?php

namespace Drupal\restrictions_by_layout\Plugin\LayoutBuilderRestriction;

use Drupal\Core\Layout\LayoutPluginManagerInterface;
use Drupal\layout_builder\SectionStorageInterface;
use Drupal\layout_builder_restrictions\Plugin\LayoutBuilderRestrictionBase;
use Drupal\layout_builder_restrictions\Traits\PluginHelperTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Controls behavior of the by class plugin.
 *
 * @TODO is all this code really needed? Seems like it should be simpler.
 *
 * @LayoutBuilderRestriction(
 *   id = "restrictions_by_layout",
 *   title = @Translation("Layout Defined Restrictions"),
 *   description = @Translation("Allow layout definitions to restrict blocks.")
 * )
 */
class RestrictionsByLayout extends LayoutBuilderRestrictionBase {

  use PluginHelperTrait;

  /**
   * The layout plugin manager.
   *
   * @var \Drupal\Core\Layout\LayoutPluginManagerInterface
   */
  protected $layoutManager;

  /**
   * Constructs a Drupal\Component\Plugin\PluginBase object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Layout\LayoutPluginManagerInterface $layoutManager
   *   The layout manager.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, LayoutPluginManagerInterface $layoutManager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);

    $this->layoutManager = $layoutManager;

  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('plugin.manager.core.layout'),
    );
  }

  /**
   * Get the layout definition.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $sectionStorage
   *   The section.
   * @param $delta
   *   The section delta.
   *
   * @return \Drupal\Core\Layout\LayoutDefinition|null
   *   The layout definition or NULL.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getLayout(SectionStorageInterface $sectionStorage, $delta) {
    $layoutId = $sectionStorage->getSection($delta)->getLayoutId();

    return $this->layoutManager->getDefinition($layoutId);
  }

  /**
   * Get restrictions for a section.
   *
   * @param \Drupal\layout_builder\SectionStorageInterface $sectionStorage
   *   The section.
   * @param $delta
   *   The section delta.
   *
   * @return array
   *   The section's restrictions.
   *
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getRestrictions(SectionStorageInterface $sectionStorage, $delta) {
    $layout = $this->getLayout($sectionStorage, $delta);

    return $layout->get('restrictions') ?? [];
  }

  /**
   * {@inheritdoc}
   */
  public function alterBlockDefinitions(array $definitions, array $context) {
    // If this method is being called by any action other than 'Add block',
    // then do nothing.
    // @TODO: Re-assess after https://www.drupal.org/node/3099121
    // has been addressed.
    if (!isset($context['delta'])) {
      return $definitions;
    }

    $restrictions = $this->getRestrictions($context['section_storage'], $context['delta']);

    if ($restrictions) {
      $region = $context['region'];
      $allowedCategories = $restrictions['allowed_block_categories'] ?? [];
      $restrictedCategories = $restrictions['restricted_categories'] ?? [];

      $allowedBlocks = $restrictions['whitelisted_blocks'] ?? [];
      $restrictedBlocks = $restrictions['blacklisted_blocks'] ?? [];

      // If restriction applies to all regions, then overwrite region
      // to 'all_regions'.
      if (isset($allowedBlocks['all_regions']) || isset($restrictedBlocks['all_regions']) || isset($restrictedCategories['all_regions'])) {
        $region = 'all_regions';
      }

      if (!empty($allowedBlocks) || !empty($restrictedBlocks) || !empty($restrictedCategories)) {
        foreach ($definitions as $delta => $definition) {
          $original_delta = $delta;
          $category = $this->getUntranslatedCategory($definition['category']);

          // Custom blocks get special treatment.
          if ($definition['provider'] == 'block_content') {
            // 'Custom block types' are disregarded if 'Custom blocks'
            // restrictions are enabled.
            if (isset($allowedBlocks[$region]['Custom blocks']) || isset($restricedBlocks[$region]['Custom blocks'])) {
              $category = 'Custom blocks';
            }
            else {
              $category = 'Custom block types';
              $delta_exploded = explode(':', $delta);
              $uuid = $delta_exploded[1];
              // @TODO probably need this.
//              $content_block_types_by_uuid = $this->getBlockTypeByUuid();
//              $delta = $content_block_types_by_uuid[$uuid];
            }
          }

          if (in_array($category, $restrictedCategories[$region])) {
            unset($definitions[$original_delta]);
          }
          elseif (isset($allowedBlocks[$region]) && in_array($category, array_keys($allowedBlocks[$region]))) {
            if (!in_array($delta, $allowedBlocks[$region][$category])) {
              // The current block is not whitelisted. Remove it.
              unset($definitions[$original_delta]);
            }
          }
          elseif (isset($restricedBlocks[$region]) && in_array($category, array_keys($restricedBlocks[$region]))) {
            if (in_array($delta, $restricedBlocks[$region][$category])) {
              // The current block is blacklisted. Remove it.
              unset($definitions[$original_delta]);
            }
          }
          elseif ($this->categoryIsRestricted($category, $allowedCategories)) {
            unset($definitions[$original_delta]);
          }
        }
      }
    }

    return $definitions;
  }

  /**
   * {@inheritdoc}
   */
  public function blockAllowedinContext(SectionStorageInterface $section_storage, $delta_from, $delta_to, $region_to, $block_uuid, $preceding_block_uuid = NULL) {
    $destRestrictions = $this->getRestrictions($section_storage, $delta_to);

    if (empty($destRestrictions)) {
      // This layout has no restrictions. Look no further.
      return TRUE;
    }

    // There ARE restrictions. Start by assuming *this* block is not restricted.
    $has_restrictions = FALSE;

    $allowedCategories = $destRestrictions['allowed_block_categories'] ?? [];
    $restrictedCategories = $destRestrictions['restricted_categories'] ?? [];

    $allowedBlocks = $destRestrictions['whitelisted_blocks'] ?? [];
    $restrictedBlocks = $destRestrictions['blacklisted_blocks'] ?? [];

    // If restriction applies to all regions, then overwrite region
    // to 'all_regions'.
    if (isset($allowedBlocks['all_regions']) || isset($restrictedBlocks['all_regions']) || isset($restrictedCategories['all_regions'])) {
      $region = 'all_regions';
    }

    // Get block information.
    // Get "from" section and layout id. (not needed?)
    $section_from = $section_storage->getSection($delta_from);
    $component = $section_from->getComponent($block_uuid)->toArray();
    $block_id = $component['configuration']['id'];
    $block_id_parts = explode(':', $block_id);

    // Load the plugin definition.
    if ($definition = $this->blockManager()->getDefinition($block_id)) {
      $category = $this->getUntranslatedCategory($definition['category']);
      if (isset($allowedBlocks[$region_to][$category]) || isset($restrictedBlocks[$region_to][$category])) {
        // If there is a restriction, assume this block is restricted.
        // If the block is whitelisted or NOT blacklisted,
        // the restriction will be removed, below.
        $has_restrictions = TRUE;
      }
      if (in_array($category, array_values($restrictedCategories[$region_to]))) {
        $has_restrictions = TRUE;
      }
      if (!isset($restrictedCategories[$region_to][$category]) && !isset($restrictedBlocks[$region_to][$category]) && !isset($allowedBlocks[$region_to][$category]) && !in_array($category, array_values($restrictedCategories[$region_to])) && $category != "Custom blocks") {
        // No restrictions have been placed on this category.
        $has_restrictions = FALSE;
      }
      else {
        // Some type of restriction has been placed.
        if (isset($allowedBlocks[$region_to][$category])) {
          // An explicitly whitelisted block means it's allowed.
          if (in_array($block_id, $allowedBlocks[$region_to][$category])) {
            $has_restrictions = FALSE;
          }
        }
        elseif (isset($restrictedBlocks[$region_to][$category])) {
          // If absent from the blacklist, it's allowed.
          if (!in_array($block_id, $restrictedBlocks[$region_to][$category])) {
            $has_restrictions = FALSE;
          }
        }
      }

      // Edge case: if block *type* restrictions are present...
      if (!empty($allowedBlocks[$region_to]['Custom block types'])) {
        $content_block_types_by_uuid = $this->getBlockTypeByUuid();
        // If no specific custom block restrictions are set
        // check block type restrict by block type.
        if ($category == 'Custom blocks' && !isset($allowedBlocks[$region_to]['Custom blocks'])) {
          $block_bundle = $content_block_types_by_uuid[end($block_id_parts)];
          if (in_array($block_bundle, $allowedBlocks[$region_to]['Custom block types'])) {
            // There are block type restrictions AND
            // this block type has been whitelisted.
            $has_restrictions = FALSE;
          }
          else {
            // There are block type restrictions BUT
            // this block type has NOT been whitelisted.
            $has_restrictions = TRUE;
          }
        }
      }
      elseif (!empty($restrictedBlocks[$region_to]['Custom block types'])) {
        $content_block_types_by_uuid = $this->getBlockTypeByUuid();
        // If no specific custom block restrictions are set
        // check block type restrict by block type.
        if ($category == 'Custom blocks' && !isset($restrictedBlocks[$region_to]['Custom blocks'])) {
          $block_bundle = $content_block_types_by_uuid[end($block_id_parts)];
          if (in_array($block_bundle, $restrictedBlocks[$region_to]['Custom block types'])) {
            // There are block type restrictions AND
            // this block type has been blacklostlisted.
            $has_restrictions = TRUE;
          }
          else {
            // There are block type restrictions BUT
            // this block type has NOT been blacklisted.
            $has_restrictions = FALSE;
          }
        }
      }
      if ($has_restrictions) {
        $bundle = $this->getValuefromSectionStorage([$section_storage], 'bundle');
        $region = $section_storage->getSection($delta_to);
        $layout_id = $region->getLayoutId();

        return $this->t("There is a restriction on %block placement in the %layout %region region for %type content.", [
          "%block" => $definition['admin_label'],
          "%layout" => $layout_id,
          "%region" => $region_to,
          "%type" => $bundle,
        ]);
      }
    }

    // Default: this block is not restricted.
    return TRUE;
  }


  /**
   * {@inheritdoc}
   */
  public function inlineBlocksAllowedinContext(SectionStorageInterface $section_storage, $delta, $region) {
    $a = 5;
    // @TODO probably need this.
//    $view_display = $this->getValuefromSectionStorage([$section_storage], 'view_display');
//    $third_party_settings = $view_display->getThirdPartySetting('layout_builder_restrictions', 'entity_view_mode_restriction_by_region', []);
//    $whitelisted_blocks = (isset($third_party_settings['whitelisted_blocks'])) ? $third_party_settings['whitelisted_blocks'] : [];
//    $blacklisted_blocks = (isset($third_party_settings['blacklisted_blocks'])) ? $third_party_settings['blacklisted_blocks'] : [];
//
//    $layout_id = $section_storage->getSection($delta)->getLayoutId();
//
//    // If restriction behavior is for all regions, then overwrite
//    // region with 'all_regions'.
//    if (isset($third_party_settings['whitelisted_blocks'][$layout_id]['all_regions']) || isset($third_party_settings['blacklisted_blocks'][$layout_id]['all_regions']) || isset($third_party_settings['restricted_categories'][$layout_id]['all_regions'])) {
//      $region = 'all_regions';
//    }
//
//    // Check if allowed inline blocks are defined in config.
//    if (isset($whitelisted_blocks[$layout_id][$region]['Inline blocks'])) {
//      return $whitelisted_blocks[$layout_id][$region]['Inline blocks'];
//    }
//    // If not, then allow some inline blocks and check for blacklisting.
//    else {
//      $inline_blocks = $this->getInlineBlockPlugins();
//      if (isset($blacklisted_blocks[$layout_id][$region]['Inline blocks'])) {
//        foreach ($inline_blocks as $key => $block) {
//          // Unset explicitly blacklisted inline blocks.
//          if (in_array($block, $blacklisted_blocks[$layout_id][$region]['Inline blocks'])) {
//            unset($inline_blocks[$key]);
//          }
//        }
//      }
//      return $inline_blocks;
//    }
  }

//  /**
//   * Helper function to retrieve uuid->type keyed block array.
//   *
//   * @return str[]
//   *   A key-value array of uuid-block type.
//   */
//  private function getBlockTypeByUuid() {
//    if ($this->moduleHandler->moduleExists('block_content')) {
//      // Pre-load all reusable blocks by UUID to retrieve block type.
//      $query = $this->database->select('block_content', 'b')
//        ->fields('b', ['uuid', 'type']);
//      $results = $query->execute();
//      return $results->fetchAllKeyed(0, 1);
//    }
//    return [];
//  }

}
